# iucode-downloader   

## Requirements    

* `bash`
* C compiler
* `curl`
* `mktemp`

## Options   

* -t= | --temp-dir=    
&nbsp;&nbsp;Set this to a directory which will be used to for writing source code and compilation. Default `use mktemp in /tmp/`.    

* -k= | --keep-temp=     
&nbsp;&nbsp;Set this to anything in order to keep temp folder. Default `not set`.     

* -c= | --compiler=   
&nbsp;&nbsp;Set this to a C compiler. Default `cc`.    

* -d= | --down-dir=    
&nbsp;&nbsp;Set this to the folder where ucode will be downloaded. Default `script location`.     

## How it works   

The script compiles and runs C code in order to get the family, model and stepping of the processor in question.  

Afterwards, the script uses `curl` to download the ucode.   

Connections are made to: `https://raw.githubusercontent.com/intel/Intel-Linux-Processor-Microcode-Data-Files/master/intel-ucode/$fam-$mod-$step`.   

The file pattern used by Intel is `family-model-step`.     

## Tested on  

* FreeBSD 12   
* Debian 9    
