#!/usr/bin/env bash

for i in "$@"
do
case $i in 
	-h|--help)
	echo "Arguments: "
	echo -e "  --temp-dir=\n  --keep-temp=\n  --compiler=\n  --down-dir=\n"
	echo -e "Example: \n  $0 --temp-dir=/tmp/dir --keep-temp=can-be-set-to-anything --compiler=/usr/bin/gcc --down-dir=/home/apple/Downloads"
	exit

	;;
	-t=*|--temp-dir=*)
	TEMP_DIR="${i#*=}"

	;;
	-c=*|--compiler=*)
	COMPILER="${i#*=}"

	;;
	-d=*|--down-dir=*)
	DOWN_DIR="${i#*=}"

	;;
	-k=*|--keep-temp=*)
	KEEP_TEMP="${i#*=}"

	;;
esac
done

if [ ! -n "$TEMP_DIR" ]; then
	TEMP_DIR=`mktemp -d` || exit 1
fi
if [ ! -n "$COMPILER" ]; then
	COMPILER="cc"
fi
if [ ! -n "$DOWN_DIR" ]; then
	DOWN_DIR=$(dirname $(readlink -f "${BASH_SOURCE[0]}") )
fi

cat > $TEMP_DIR/info.c << 'EOL'
#include <stdio.h>

int main(void)
{
  unsigned int out;
  unsigned short int fam,mod,step,extmod,extfam,dispmod,dispfam;
  __asm__("cpuid"
          :"=a"(out) // EAX into out
          :"0"(1) // 1 into EAX
          :"%ebx","%ecx","%edx");
fam = (out>>8) &0xF;
mod = (out>>4) &0xF;
step = out &0xF;
extmod = (out>>16) &0xF;
extfam = (out>>20) &0xFF;


if (fam == 6 || fam == 15) {
        dispmod = (extmod<<4) + mod;
} else {
        dispmod = mod;
}
if (fam == 15) {
        dispfam = extfam + fam;
} else {
        dispfam = fam;
}

printf ("%02x-%02x-%02x\n",dispfam,dispmod,step) ;

return 0;
}
EOL

cd $TEMP_DIR/
$COMPILER $TEMP_DIR/info.c
CPUID_FMS=$($TEMP_DIR/a.out)

if [ ! -n "$KEEP_TEMP" ]; then
	rm -rf $TEMP_DIR/
fi

cd $DOWN_DIR
echo Downloading to $DOWN_DIR/firmware_$CPUID_FMS.bin ...
curl -o $DOWN_DIR/firmware_$CPUID_FMS.bin -L https://raw.githubusercontent.com/intel/Intel-Linux-Processor-Microcode-Data-Files/master/intel-ucode/$CPUID_FMS
